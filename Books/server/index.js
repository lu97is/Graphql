const express = require('express');
const graphqlHTPP = require('express-graphql');
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT ||8080;
const schema = require('./schema/schema');

mongoose.connect('mongodb://lu97is:eagle1997@ds261440.mlab.com:61440/graphqlbooks');
mongoose.connection.once('open', () => {
    console.log('Conected')
})


app.use('/graphql', graphqlHTPP({
    schema,
    graphiql: true
}));
app.listen(port, ()=> {
    console.log('app runing on', port)
});